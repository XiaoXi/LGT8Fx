# LGT8Fx

⭐ Logic Green 的 LGT8F328P、LGT8F328D、LGT8F88D 开发板 Arduino 资源包 ⭐

## 📦️ 安装

1. 前往 `文件` -> `首选项` -> `附加开发板管理器地址` 添加以下开发板管理器地址
`https://gitlab.soraharu.com/XiaoXi/LGT8Fx/-/raw/master/package_lgt8fx_index.json`
2. 前往 `工具` -> `开发板` -> `开发板管理器` 搜索 `lgt8fx` 并安装 `LGT8Fx Boards`
3. 现在，LGT8F 系列开发板已经出现在你的 IDE 中，你可以继续选择 `时钟速度` 等参数

32MHz 的速度是传统 Arduino Uno 的两倍！与 ATmega 328P 相比，许多操作都将占用更少的时钟周期，这意味着它们将运行得更快。

## 📃 与原始核心的差异 [Larduino_HSP v3.6c](https://github.com/Edragon/LGT/tree/master/1-LGT8F328/SDK/Arduino/HSP/HSP%20Patch%20File/Larduino_HSP_3.6c/Larduino_HSP_v3.6c)

- 支持 32MHz 以及其他时钟速度
- 差分放大器 API
- 更好的开发板菜单
- 通过 开发板管理器地址 安装
- SoftwareSerial @32MHz
- 从 https://github.com/LGTMCU/Larduino_HSP 移植的 FastIO

## 🔋 能源消耗 @ 5v

| Clock | Pro mini style w/o power LED | Pro mini style | Nano style |
| ----- | ---------------------------- | -------------- | ---------- |
| 32MHz | 12.7mA                       | 15.0mA         | 32.6mA     |
| 16MHz | 9.2mA                        | 11.5mA         | 27.8mA     |
| 8MHz  | 7.1mA                        | 9.4mA          | 25.4mA     |
| 4MHz  | 5.9mA                        | 8.2mA          | 23.3mA     |
| 2MHz  | 5.3mA                        | 7.6mA          | 23.4mA     |
| 1MHz  | 5.0mA                        | 7.3mA          | 22.8mA     |

## 🚫 免责声明

我与 Arduino、Logic Green、Atmel 等公司没有任何关联。我只是想提供一种便捷的方式来使用这些开发板，并使其以最大速度运行且不会受到黑客的攻击。

## 💕 感谢

- [#Larduino_HSP](https://github.com/LGTMCU/Larduino_HSP) 本项目 99.9999％ 基于该项目

## 📜 开源许可

基于 [WTFPL](https://choosealicense.com/licenses/wtfpl/) 许可进行开源。
